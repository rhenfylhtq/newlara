<!-- Bootstrap core JavaScript
================================================= -->
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


<script>
    window.document.addEventListener("DOMContentLoaded", function () {
        let button = document.querySelector('#passwordHide');
        button.addEventListener('click', function () {
            let input = document.querySelector("[name='password']");
            let type = input.getAttribute('type');
            if (type === 'password') {
                type = 'text';
                this.innerText = 'Hide';
            } else {
                type = 'password';
                this.innerText = 'Show';
            }
            input.setAttribute('type', type);
        });
    });
</script>
