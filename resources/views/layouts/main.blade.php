<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.partials.head')
</head>
<body>
@include('layouts.partials.nav')
@include('layouts.partials.header')
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @yield('content')

</div>
@include('layouts.partials.footer')
@include('layouts.partials.footer-scripts')
 </body>
</html>
