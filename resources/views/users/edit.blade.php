@extends('layouts.main')
@section('content')
    <form method="POST" action="{{route('users.edit', $user->id)}}">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="exampleInputName" class="form-label">Имя пользователя</label>
            <input name="name" type="text" class="form-control" id="exampleInputName" value="{{ old('name', $user->name) }}" required>
            <span class="help-text"></span>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Электронная почта</label>
            <input name="email" type="email" class="form-control" value="{{ old('email', $user->email) }}" id="exampleInputEmail1" aria-describedby="emailHelp" required>
        </div>

        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Пароль</label>
            <div class="input-group mb-3">
                <input  name="password" type="password"  class="form-control"value="{{ old('password',$user->password) }}" id="exampleInputPassword1" required aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" id="passwordHide" type="button">Show</button>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Изменить пользователя</button>
    </form>

@endsection
