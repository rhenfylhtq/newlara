@extends('layouts.main')
@section('content')

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Updated at</th>
            <th scope="col">Created at</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->updated_at}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    @if($user->trached())
                        <form action="{{route('users.restore', $user)}}">
                            <button class="btn btn-outline-primary btn-xs btn-block">Восстановить</button>
                        </form>
                    @endif
                    <form action="{{route('users.show', $user)}}">
                        <button class="btn btn-outline-warning btn-xs btn-block">Изменить</button>
                    </form>
                    <br/>
                    <form action="{{route('users.destroy', $user)}}" method="POST" >
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger btn-xs btn-block">Удалить</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
@endsection
