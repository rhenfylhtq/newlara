@extends('layouts.main')
@section('content')
    <div class="input-group mb-3">
        <a href="{{route('users.create')}}" class="btn btn-success">Создать пользователя</a>
    </div>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <form role="form" action="{{route('users.index')}}">
            <input type="text" class="form-control" name="name" placeholder="Поиск... "
                   value="{{old('name')}}" aria-label="Text input with dropdown button">
            <select name="email">
                <option value="">Email</option>
                @foreach($mailDomains as $mailDomain)
                    <option value="{{$mailDomain}}">{{$mailDomain}}</option>
                @endforeach
            </select>
            <select name="activity">
                <option value="all">Все</option>
                <option value="active" selected>Активный</option>
                <option value="inactive">Удаленный</option>
            </select>
            <button type="submit">Поиск</button>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Updated at</th>
            <th scope="col">Created at</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row"><a href="{{route('user.view', $user)}}">{{$user->id}}</a></th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->updated_at}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    @if($user->trashed())
                        <form method="POST" action="{{route('users.restore', $user)}}">
                            <button class="btn btn-outline-primary btn-xs btn-block">Восстановить</button>
                            @csrf
                        </form>
                        @else
                    <form action="{{route('users.show', $user)}}">
                        <button class="btn btn-outline-warning btn-xs btn-block">Изменить</button>
                    </form>
                    <br/>
                    <form action="{{route('users.destroy', $user)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger btn-xs btn-block">Удалить</button>
                    </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
