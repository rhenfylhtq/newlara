@extends('layouts.main')
@section('content')
    <form method="POST" action="{{route('users.store')}}">
        @csrf
        <div class="mb-3">
            <label for="exampleInputName" class="form-label">Имя пользователя</label>
            <input name="name" type="text" class="form-control" id="exampleInputName"  value="{{ old('name') }}">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Электронная почта</label>
            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  value="{{ old('email') }}">
        </div>

        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Пароль</label>
            <div class="input-group mb-3">
                <input  name="password" type="password"  class="form-control" value="{{ old('password') }}" id="exampleInputPassword1" required aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" id="passwordHide" type="button">Show</button>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Создать пользователя</button>
    </form>
@endsection
