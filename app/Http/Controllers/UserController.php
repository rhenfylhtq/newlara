<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSearchRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(UserSearchRequest $request)
    {
        $query = User::query();

        switch ($request->get('activity')) {
            case 'all':
                $query->withTrashed();
                break;
            case 'inactive':
                $query->onlyTrashed();
        }

        $name = $request->get('name');
        if ($name) {
            $query->whereName($name);
        }

        $query->where('email', 'like','%' . $request->get('email'));

        return view('users.index', [
            'users' => $query->get(),
            'mailDomains' => DB::table('users')
                ->selectRaw("distinct right(email, length(email)-INSTR(email, '@')) as mail_domain")
                ->get()
                ->pluck('mail_domain'),
        ]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(UserStoreRequest $request)
    {
        $user = new User();
        $user->fill($request->validated());
        $user->save();
        return redirect(\route('users.index'));
    }

    public function show(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function edit(User $user, UserUpdateRequest $request)
    {
        $user->update($request->validated());
        return redirect(\route('users.index'));
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect(\route('users.index'));
    }

    public function restore(User $user)
    {
        User::query()
            ->restore($user->id);
        return redirect(\route('users.index'));
    }

    public function view(User $user)
    {
        return view('users.view', ['user' => $user]);
    }
}
